#!/usr/bin/env bash

#######################################################################################
# map_to_tiles.sh
# Author: Peter Refsing Andersen 
# Contact: peter.andersen@imba.oeaw.ac.at

# Load required modules:
module load bedtools/2.25.0 # Note: bedtools/2.26.0 does not work with this script due to groupby acting wierdly!


usage()

{
  cat << EOF
  
  
  Help menu for $0
  
  #########################################################################################################################################
  This script quantifies 1kb window coverage from NGS data analyzed by the new 'Annotation Pipeline' (by Dominik Handler)
  
  usage: map_to_tiles.sh [options] -i
  
  OPTIONS:
      -h  Show this message
    
      -i  Name of the input folder (annotation pipeline output folder - see the readme file for format details).      		

      -v  Flybase version of genome annotation ["dm3" or "dm6", default = "dm6"] # only dm6 fully implemented so far!
      
      -t  Leave temporary files after completion (will otherwise be deleted by default)

   Examples usage:
   map_to_tiles.sh -i ./data/Moonshiner_sRNAseq_KO_data/ -v dm6


  #########################################################################################################################################          


EOF
}


folder=
version="dm6"
cleanup="Y"

while getopts “hi:v:t,” OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    i)
      folder=$OPTARG
      ;;
    v)
      version=$OPTARG
      ;;
    t)
      cleanup="N"
      ;;
    ?)
      usage
      exit
      ;;
  esac
done

###################################################################################################
##check if variables are filled or create optional variables
if [[ -z $folder ]]
then 
    echo  -i not set
    usage
    exit 1
fi

# Check if last character of the folder name is "/". If not then add it:
[[ $folder != *\/ ]] && folder=$folder"/"

if [[ ! $version == "dm3" ]] && [[ ! $version == "dm6" ]]
then
  printf "\n\n\n  ERROR! Version of genome annotation is not set to dm3 or dm6\n\n"
  usage
  exit 1
fi


### Create final location variables

# Include git hash (if exists) in output folder name:
SCRIPTPATH=$( cd "$(dirname "$0")" ; pwd -P ) # Get absolute script path
if [ -d ${SCRIPTPATH}"/.git" ]; then # Test if script path is a git repository
  HASH=$( cd ${SCRIPTPATH}; git rev-parse --short HEAD ) # Fetch Git hash
  output_folder=${folder}"IntersectWindows_"${HASH}/
else
  HASH="note: this script not in git repository"
  output_folder=${folder}"IntersectWindows"/
fi;
echo "Git hash: "$HASH
mkdir -p $output_folder


if [[ $version == "dm6" ]] 
then
  tile_map=${SCRIPTPATH}"/kb_tile_files/dm6_1kb_windows_DNAseq_mappability_mainchr_sorted.txt"
else
  printf "\n\n\nversion is not set to dm6; note: dm3 genome assembly not yet implemented!\n\n\n"
  usage
  exit 1
fi

# Other folders and variables:
input_folder=${folder}"individual-libraries"/
intersect_tmp=${output_folder}"intersect_tmp.txt"
input_tmp=${output_folder}"in_temp.txt"
anno_file=${folder}"annotation_counts.txt"
samp_name=$(basename $folder)
echo "Sample set being processed:" $samp_name
echo "Performing temp clean-up? :" $cleanup


###################################################################################################
# 1: Extracting unique mappers longer than 22 nt:
printf "\n\n---------------------------------------------------------------------------------------------------------\n"
printf "1: Extracting unique mappers longer than 22 nt:\n\n"

for FILE in $(find $input_folder -name "*annotated.bed.gz") # Find annotated mapping files produced by the annotation pipeline
do
  echo "Processing " $(basename $FILE)
  OUT=$(basename $FILE | sed -e "s/.bed.gz/.unique.bed.gz/") # Create output file name
  if [ ! -e ${output_folder}$OUT ] # Test if output file exists
  then
    zcat $FILE | awk '($4~"mapping=u") && ($4!~"ann=miRNA|ann=mito|ann=rRNA|ann=snoRNA|ann=snRNA|ann=tRNA") && ($3-$2 > 22) {print}' | gzip > ${output_folder}$OUT # Copy lines from reads that have been uniquely mapped ($4~"@u"), do not arise from abundant ncRNA loci ($4!~"...") and are longer than 22 nt ($3-$2 > 22)
    echo $OUT ' done!'
  else
    echo $OUT already exists - skipping
  fi
done

###################################################################################################
# 2: Intersecting unique mappers with the bed file containing 1kb windows:
printf "\n\n---------------------------------------------------------------------------------------------------------\n"
printf "2: Intersecting unique mappers with the bed file containing 1kb windows:\n\n"

# The following was inspired by this page: https://www.biostars.org/p/61044/
# See also:
# http://bedtools.readthedocs.org/en/latest/content/tools/intersect.html
# http://bedtools.readthedocs.org/en/latest/content/tools/groupby.html

for FILE in $(find $output_folder -name "*annotated.unique.bed.gz")
do
  echo "Processing plus strand mappers of" $(basename $FILE)
  OUT=$(echo $FILE | sed -e "s/annotated.unique.bed.gz/WindowCounts_tempPlus.txt/") # Create output file name
  if [ ! -e $OUT ] # Test if output file exists
  then
    zcat $FILE | awk '{ $1="chr"$1; if ($6 == "+") print($1,$2,$3,"X",$5,$6) }' OFS="\t" | sort -k1,1 -k2,2n > $input_tmp # Print and sort data from reads mapping to the + strand
    bedtools intersect -a $tile_map -b $input_tmp -sorted -wao -F 0.5 > $intersect_tmp # Intersect reads with 1kb windows and require mapping of half the sRNA to minimize double counting.
		col_count=$(awk '{print NF; exit}' $intersect_tmp) # Count the number of columns in the intersect_tmp file (used to ID which columns to group by in the next step.
    group_sum_col=`expr $col_count - 2` # set to denote the summary column of the table for the groupby command below
    group_end_col=`expr $col_count - 7` # set to exclude the last columns from the intersection in the grouping parameters
    cat $intersect_tmp | bedtools groupby -g 1-${group_end_col} -c ${group_sum_col} -o sum > $OUT # Sum the number of reads mapping to each window and write to temp file
    echo 'done!'
  else
    echo $(basename $OUT) already exists - skipping
  fi

  echo "Processing minus strand mappers of" $(basename $FILE)
  OUT=$(echo $FILE | sed -e "s/annotated.unique.bed.gz/WindowCounts_tempMinus.txt/")
  if [ ! -e $OUT ]
  then
    zcat $FILE | awk '{ $1="chr"$1; if ($6 == "-") print($1,$2,$3,"X",$5,$6) }' OFS="\t" | sort -k1,1 -k2,2n > $input_tmp # Print and sort data from reads mapping to the - strand
    bedtools intersect -a $tile_map -b $input_tmp -sorted -wao -F 0.5 > $intersect_tmp # Intersect reads with 1kb windows and require mapping of half the sRNA to minimize double counting.
		col_count=$(awk '{print NF; exit}' $intersect_tmp) # Count the number of columns in the intersect_tmp file (used to ID which columns to group by in the next step.
    group_sum_col=`expr $col_count - 2` # set to denote the summary column of the table for the groupby command below
    group_end_col=`expr $col_count - 7` # set to exclude the last columns from the intersection in the grouping parameters
    cat $intersect_tmp | bedtools groupby -g 1-${group_end_col} -c ${group_sum_col} -o sum > $OUT # Sum the number of reads mapping to each window and write to temp file
    echo -e 'done!\n'
  else
    echo $(basename $OUT) already exists - skipping
  fi
done


###################################################################################################
# 3: Assembling tables from all the produced intersections:
printf "\n\n---------------------------------------------------------------------------------------------------------\n"
printf "3: Assembling tables from all the produced intersections:\n\n"

col_count=$(awk '{print NF; exit}' $intersect_tmp) # Count the number of columns in the intersect_tmp file

# Plus strand count extraction:
for FILE in $(find $output_folder -name "*WindowCounts_tempPlus.txt")
do
  echo "Processing " $(basename $FILE)
  OUT=$(echo $FILE | sed -e "s/_WindowCounts_tempPlus.txt/_tempP.txt/") # Create output file name
  if [ ! -e $OUT ] # Test if output file exists
  then
    cut -f $(($col_count - 6)) $FILE > $OUT
    echo $(basename $OUT) ' done!'
  else
    echo $(basename $OUT) already exists - skipping
  fi
done

# Minus strand count extraction:
for FILE in $(find $output_folder -name "*WindowCounts_tempMinus.txt")
do
  echo "Processing " $(basename $FILE)
  OUT=$(echo $FILE | sed -e "s/_WindowCounts_tempMinus.txt/_tempM.txt/")
  if [ ! -e $OUT ]
  then
    cut -f $(($col_count - 6)) $FILE > $OUT
    echo $(basename $OUT) ' done!'
  else
    echo $(basename $OUT) already exists - skipping
  fi
done

# Assembling tables:
#	Find the files for each sample	   # sort and paste to lines # exchange -1 with 0 and write to a file
find $output_folder -name "*_tempP.txt" | sort -r | xargs paste | sed 's/-1/0/g' > ${output_folder}AlltempPastedPlus.txt
find $output_folder -name "*_tempM.txt" | sort -r | xargs paste | sed 's/-1/0/g' > ${output_folder}AlltempPastedMinus.txt


###################################################################################################
# 4: Adding headers and normalization numbers...
printf "\n\n---------------------------------------------------------------------------------------------------------\n"
printf "4: Adding headers and normalization numbers\n\n"

# Make header
find $output_folder -name "*_tempPlus.txt" | sort -r | cut -c 1- | sed -e "s,_WindowCounts_tempPlus.txt,," | sed -e "s,$output_folder,," |
tr "\n" "\t" > ${output_folder}SampleHeaders.txt
echo "" >> ${output_folder}SampleHeaders.txt

# Make normalization factor file based on the normalization factors applied for producing the track hubs (last line of normalization.txt: 

NORM_FACTORS=""
for xy in $(cat ${output_folder}SampleHeaders.txt) # Loop over all samples
do 
	currNORM_FACTOR=$(tail -n 1 "${input_folder}/${xy}/normalization.txt" | head -n 1 | tr " " "\t" | cut -f 1 ) # fetch the applied normalization number
	NORM_FACTORS="${NORM_FACTORS} $currNORM_FACTOR" # Add the fetched number to the list
done
NORM_FACTORS=$( echo $NORM_FACTORS | tr ' ' '\t' ) # transpose the list from line-separated to tab-separated
echo $NORM_FACTORS # print the normalization factors to std out
echo $NORM_FACTORS > ${output_folder}NORMcounts.txt # Save the normalization factors to a file

# Calculate sums of window mapped reads from each library:
awk '{ for(i = 1; i <= NF; i++) sum[i]+=$i;} END { for(i = 1; i <= NF; i++) print sum[i]}' ${output_folder}AlltempPastedPlus.txt | tr "\n" "\t" | sed '$a\' > ${output_folder}Mapped_reads_plus.txt
awk '{ for(i = 1; i <= NF; i++) sum[i]+=$i;} END { for(i = 1; i <= NF; i++) print sum[i]}' ${output_folder}AlltempPastedMinus.txt | tr "\n" "\t" | sed '$a\' > ${output_folder}Mapped_reads_minus.txt


# Combine temp files, header and normcounts
cat ${output_folder}SampleHeaders.txt ${output_folder}NORMcounts.txt ${output_folder}Mapped_reads_plus.txt ${output_folder}AlltempPastedPlus.txt > ${output_folder}"WindowCounts_"${samp_name}"_Plus.txt"
cat ${output_folder}SampleHeaders.txt ${output_folder}NORMcounts.txt ${output_folder}Mapped_reads_minus.txt ${output_folder}AlltempPastedMinus.txt > ${output_folder}"WindowCounts_"${samp_name}"_Minus.txt"

###################################################################################################
# 5: Cleaning up temporary files...
if [[ $cleanup == Y ]]
then
  printf "\n\n---------------------------------------------------------------------------------------------------------\n"
  printf "5: Cleaning up temporary files...\n\n"
  rm ${output_folder}*temp*
fi

# Find the file here: 
printf "\n\n---------------------------------------------------------------------------------------------------------\n"
printf "Done! Find the output files here:\n"
printf ${output_folder}"WindowCounts_"${samp_name}"_Plus.txt\n"
printf ${output_folder}"WindowCounts_"${samp_name}"_Minus.txt\n"

exit 
