#!/usr/bin/env bash

#######################################################################################
# tile_framework_buildup.sh
# Author: Peter Refsing Andersen 
# Contact: peter.andersen@imba.oeaw.ac.at

# Load required modules:
module load bedtools/2.25.0 # Note: bedtools/2.26.0 does not work with this script due to groupby acting wierdly!

#set -u # Treat unset variables as an error when substituting.

#######################################################################################
# 1: Set up variables:

MAIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # get script directory
DATA_DIR=${MAIN_DIR}/data/
OUT_DIR=${MAIN_DIR}/kb_tile_files/
TMP=${OUT_DIR}tmp/

mkdir -p ${TMP}
mkdir -p ${OUT_DIR}

echo >> ${OUT_DIR}log.txt
echo "This is the log file for the 'Framework generation for dm6 1 kb tile analyses.sh' script" > ${OUT_DIR}log.txt
echo "This script was run on: " $(date +%F-%Hh%Mm) >> ${OUT_DIR}log.txt
echo >> ${OUT_DIR}log.txt

echo "Used directories:" >> ${OUT_DIR}log.txt
echo ${MAIN_DIR} >> ${OUT_DIR}log.txt
echo ${DATA_DIR} >> ${OUT_DIR}log.txt
echo ${OUT_DIR} >> ${OUT_DIR}log.txt
echo ${TMP} >> ${OUT_DIR}log.txt
echo >> ${OUT_DIR}log.txt

# Predicted mappability scores for each 1 kb tile of the dm6 genome:
mappabilityTable=${DATA_DIR}mappability_1kb-tiles.txt

# Manual annotation of the major piRNA clusters in dm6:
piRNAclusterAnnotation=${DATA_DIR}2016-05-17_dmel_r6_clusters.bed

# Manual annotation of the major piRNA cluster promoters (DNA-encoded) in dm6:
piRNAclusterPromoterAnnotation=${DATA_DIR}2016-05-17_dmel_r6_clusterTSS.bed

# The annotation file, dmel_r6_refseq_genes_fulltable.txt, was downloaded from UCSC table
# browser > dmel_r6; RefSeq Genes; refGene table; output format: all fields
annotationTable=${DATA_DIR}dmel_r6_refseq_genes_fulltable.txt


#######################################################################################
# 2: Prepare tile mappability file:

echo
echo "* Preparing window mappability file... "
echo

# Reformat the mappability table:
awk -F":|-"  'NR!=1{print $1,$2,$3,$(NF+1)=$1":"$2}' OFS="\t" ${mappabilityTable} > ${TMP}dm6_1kb_windows_DNAseq_mappability.txt

# Make version with header to keep info
awk 'BEGIN {print "chr\tstart\tend\tuniq_+\tuniq_-\tall_+\tall_-\tid"} {print}' ${TMP}dm6_1kb_windows_DNAseq_mappability.txt > ${TMP}dm6_1kb_windows_DNAseq_mappability_header.txt

# Remove all non-main chromosomes:
awk -F '\t' 'length($1) < 6 {print}' ${TMP}dm6_1kb_windows_DNAseq_mappability.txt > ${TMP}dm6_1kb_windows_DNAseq_mappability_mainchr.txt

# Sort file. [maybe not necessary, but just in case - see next command]
cat ${TMP}dm6_1kb_windows_DNAseq_mappability_mainchr.txt | sort -k1,1 -k2,2n > ${OUT_DIR}dm6_1kb_windows_DNAseq_mappability_mainchr_sorted.txt
# Test if sorted file is different from the pre-sorted:
# cmp --silent KbWindows_ClusterAnnotated_main_dm6.txt KbWindows_ClusterAnnotated_main_dm6_sorted.txt || echo "files are different"

# Count windows per chromosome (sanity check 1):
echo "Number of 1 kb tiles from each chromosome (sanitiry check):" >> ${OUT_DIR}log.txt
echo $(awk -F '\t' '{print $1}' ${OUT_DIR}dm6_1kb_windows_DNAseq_mappability_mainchr_sorted.txt | sort | uniq -c | sort -nr) >> ${OUT_DIR}log.txt
echo >> ${OUT_DIR}log.txt

## Find duplicate values in a column (sanity check 2):
# http://www.unix.com/shell-programming-and-scripting/222103-check-identify-duplicate-values-first-column-csv-file.html
echo "A search for duplicated values: " >> ${OUT_DIR}log.txt
echo $(awk ' ++A[$8] > 1 { print $8 " is duplicated"} ' ${OUT_DIR}dm6_1kb_windows_DNAseq_mappability_mainchr_sorted.txt) >> ${OUT_DIR}log.txt
echo >> ${OUT_DIR}log.txt


#######################################################################################
# 3: Prepare window annotation file:

echo "* Preparing annotation file..."
echo

# Create gene annotation file:
tail -n+2 ${annotationTable} > ${TMP}fulltable_noheader.txt
cat ${TMP}fulltable_noheader.txt | awk '{ FS = OFS = "\t" } {print $3,$5,$6,$2,$13,$4}' > ${TMP}refGene_table.bed
cat ${TMP}refGene_table.bed | sort -k1,1 -k2,2n > ${TMP}refGene_table_sorted.bed
cat ${TMP}refGene_table.bed ${piRNAclusterAnnotation} | sort -k1,1 -k2,2n > ${OUT_DIR}refGene_table_wClusters_sorted.bed

# Create TSS annotation file
cat ${TMP}refGene_table_sorted.bed | awk '$6 == "+"' | awk 'BEGIN {FS = OFS = "\t"} {print $1,$2,$2+1,$4,$5,$6}' > ${TMP}tssplus.bed
cat ${TMP}refGene_table_sorted.bed | awk '$6 == "-"' | awk 'BEGIN {FS = OFS = "\t"} {print $1,$3,$3+1,$4,$5,$6}' > ${TMP}tssminus.bed
cat ${TMP}tssplus.bed ${TMP}tssminus.bed | sort -k1,1 -k2,2n > ${TMP}refTSS_table_sorted.bed

# Add cluster TSS annotations:
cat ${TMP}refTSS_table_sorted.bed ${piRNAclusterPromoterAnnotation} | sort -k1,1 -k2,2n > ${OUT_DIR}refTSS_table_wClusters_sorted.bed



#######################################################################################
# 3: Intersect tiles with annotations:

echo "* Intersecting tiles and annotations..."
echo

# Intersect windows with gene/cluster annotations:
bedtools intersect -a ${OUT_DIR}dm6_1kb_windows_DNAseq_mappability_mainchr_sorted.txt -b ${OUT_DIR}refGene_table_wClusters_sorted.bed \
-wao | cut -f 1-8,12,13 | groupBy -g 1-8 -c 9,10 -o distinct > ${OUT_DIR}kbWindows_mainchr_genes_dm6.txt

# Intersect windows with TSS annotations:
bedtools intersect -a ${OUT_DIR}kbWindows_mainchr_genes_dm6.txt -b ${OUT_DIR}refTSS_table_wClusters_sorted.bed \
-wao | cut -f 1-10,14,15 | groupBy -g 1-10 -c 11,12 -o distinct > ${OUT_DIR}kbWindows_mainchr_genes_tss_dm6.txt

# Count the number of tiles per chromosome:
tile_numbers=$(awk -F '\t' '{print $1}' ${OUT_DIR}kbWindows_mainchr_genes_tss_dm6.txt | sort | uniq -c | sort -nr | awk '{print $2,$1}')

echo "* Done!"
echo
echo "The annotated tile file can be found here: "${OUT_DIR}kbWindows_mainchr_genes_tss_dm6.txt
echo
echo "Sanity check:"
echo "Number of tiles per chromosome:"
echo "$tile_numbers"
echo
echo "Length of each main chromosome in the used genome assembly:"
head ${DATA_DIR}chrom.sizes.dm6 -n 7 | sort -k2,2nr
echo



