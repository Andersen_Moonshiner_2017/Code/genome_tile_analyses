# Project Title

This project contains examples of the main scripts used for the genomic tile analyses in
the Moonshiner paper (Andersen et al, 2017, Nature; PMID:28847004)

## Getting Started

Just clone the git. Test data are included.


### Prerequisites

The bash scripts require bedtools/2.25.0 installed in a standard bash/unix environment
The R script requires the installation of the following R packages:
```
install.packages(ggplot2)
install.packages(scales)
install.packages(reshape2)
install.packages(tidyverse)
```

## Running the test dataset

This git includes small RNA-seq data required to reproduces figure 4a of the Moonshiner
paper. To do this perform the following:
```
1. Run "tile_framework_buildup.sh" to build up the genome tile framework
2. Run "map_to_tiles.sh -i ./data/Moonshiner_sRNAseq_KO_data/ -v dm6"
3. Run "tile_analyses_sRNAseq_KO.R" using e.g. RStudio
```

### Input data format

The sequencing input data format for the map_to_tiles.sh script is a modified bed6 format
with the following important features:

```
Field 4 contains a "mapping=" string followed by either a "u" (unique mapper) or an "m" (multimapper)
Field 4 contains a "ann=" string followed by various annotation categories that are here used to filter away reads mapping to ncRNA loci
```

## Authors

* **Peter Refsing Andersen**


## Acknowledgments

* These scripts rely on the output from the NGS annotation pipeline written by Dominik Handler in the Brennecke lab

